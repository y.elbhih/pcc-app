package fr.lmep.pcc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PCCAppServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PCCAppServiceApplication.class, args);
	}

}
