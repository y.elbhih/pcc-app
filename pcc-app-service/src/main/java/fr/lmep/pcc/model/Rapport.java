package fr.lmep.pcc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rapport")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Rapport {

	@Id
	@GeneratedValue
	private Long id;
	private String name;

}
