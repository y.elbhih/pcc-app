package fr.lmep.pcc.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.lmep.pcc.model.Rapport;
import fr.lmep.pcc.repository.RapportRepository;

@RestController
@RequestMapping("/rapports")
public class RapportController {

	@Autowired
	private RapportRepository rapportRepository;

	@GetMapping
	public List<Rapport> getRapports() {
		return rapportRepository.findAll();
	}

	@GetMapping("/{id}")
	public Rapport getRapport(@PathVariable Long id) {
		return rapportRepository.findById(id).orElseThrow(RuntimeException::new);
	}

	@PostMapping
	public ResponseEntity createRapport(@RequestBody Rapport rapport) throws URISyntaxException {
		Rapport savedRapport = rapportRepository.save(rapport);
		return ResponseEntity.created(new URI("/rapports/" + savedRapport.getId())).body(savedRapport);
	}

	@PutMapping("/{id}")
	public ResponseEntity updateClient(@PathVariable Long id, @RequestBody Rapport rapport) {
		Rapport currentRapport = rapportRepository.findById(id).orElseThrow(RuntimeException::new);
		currentRapport.setName(rapport.getName());
		currentRapport = rapportRepository.save(rapport);

		return ResponseEntity.ok(currentRapport);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity deleteClient(@PathVariable Long id) {
		rapportRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}

}
