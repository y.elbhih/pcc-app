package fr.lmep.pcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.lmep.pcc.model.Rapport;

@Repository
public interface RapportRepository extends JpaRepository<Rapport, Long> {

}
