import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import TodoApp from './components/todo/TodoApp'
import TodoComponent from './components/todo/TodoComponent'
import './App.css';
import './bootstrap.css';

class App extends Component {
  state = {
    rapports: []
  };

  async componentDidMount() {
    const response = await fetch('/rapports');
    const body = await response.json();
    this.setState({rapports: body});
  }

  render() {
    const {rapports} = this.state;
    return (
        <div className="App">
          <header className="App-header">
            <div className="App-intro">
              <h2>Rapports</h2>
              {rapports.map(rapport =>
                  <div key={rapport.id}>
                    {rapport.name} 
                  </div>
              )}
            </div>
          </header>
        </div>
    );
  }
}
export default App;