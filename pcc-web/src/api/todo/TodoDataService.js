import axios from 'axios'
import { RAPPORTS_API_URL } from '../../Constants'

class TodoDataService {

    getRapports() {
        return axios.get(`${RAPPORTS_API_URL}`);
    }

    getTodo(id) {
        //console.log('executed service')
        return axios.get(`${RAPPORTS_API_URL}/${id}`);
    }


    searchTodo(pageRequest) {
        //console.log('executed service')
        return axios.post(`${RAPPORTS_API_URL}/search`, pageRequest, { headers: {
                'Content-Type': 'application/json'
            }});
    }

    deleteTodo(id) {
        //console.log('executed service')
        return axios.delete(`${RAPPORTS_API_URL}/${id}`);
    }

    patchTodo(id, patch) {
        //console.log('executed service')
        return axios.patch(`${RAPPORTS_API_URL}/${id}`, patch, { headers: {
                'Content-Type': 'application/json-patch+json'
            }});
    }

}

export default new TodoDataService()