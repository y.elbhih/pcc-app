# FrontEnd ReactJS Application
Projet front for PCC

## Build Instructions
Project has `pom` file so it can be build the maven command
````
mvn clean verify
````
then jar in the target folder can be run 
````
cd target
npm start
````

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
